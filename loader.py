import abc
import csv
from json import load


class Loader(abc.ABC):

    def __init__(self):
        self._data = []

    def load(self):
        return self._data

    @abc.abstractmethod
    def formPath(self, path):
        pass


class CSVLoader(Loader):

    def formPath(self, path):
        with open(path, encoding='utf-8') as csvf:
            csvReader = csv.DictReader(csvf)
            for rows in csvReader:
                self.__replceNullToNone(rows)
                self._data.append(rows)

    def __replceNullToNone(self, entity):
        for k, v in entity.items():
            if(v in {'NULL', 'null', 'Null'}):
                entity[k] = None


class JSONLoader(Loader):

    def formPath(self, path):
        with open(path, encoding='utf-8') as jsonf:
            self._data.extend(load(jsonf))
