import getopt
import sys
from json import dumps, loads
from pathlib import Path

import requests

from bcolors import bcolors
from loader import CSVLoader, JSONLoader


def uploadToStrapi(strapiUrl, entityName, entities):
    url = f'{strapiUrl}/{entityName}'
    for entity in entities:
        resp = requests.get(f'{url}/{entity["id"]}')
        if resp.status_code == requests.codes.not_found:
            createEntity(url, entity)
        else:
            respEntity = loads(resp.text)
            if isNeedUpdate(entity, respEntity):
                updateEntity(url, entity)
            # else:
                # print(f'ignore {dumps(entity)} dosent need update')


def isNeedUpdate(entity, respEntity):
    for k, v in entity.items():
        if f'{v}' != f'{respEntity[k]}':
            return True
    return False


def createEntity(url, entity):
    createResp = requests.post(url, entity)
    print(f'{bcolors.OKBLUE}create{bcolors.ENDC} {dumps(entity)}')
    printRestonseInfo(createResp)
    return createResp


def updateEntity(url, entity):
    updateResp = requests.put(f'{url}/{entity["id"]}', entity)
    print(f'{bcolors.OKBLUE}update{bcolors.ENDC} {dumps(entity)}')
    printRestonseInfo(updateResp)
    return updateResp


def printRestonseInfo(response):
    statusColor = bcolors.OKBLUE if response.status_code == requests.codes.ok else bcolors.FAIL
    print(f'{statusColor}{response.status_code}{bcolors.ENDC} {response.text}')


def main():
    argv = sys.argv[1:]
    strapiUrl = ''
    filePath = ''

    try:
        opts, args = getopt.getopt(argv, "u:p:", ["url=", "path="])
    except getopt.GetoptError:
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-u", "--url"):
            strapiUrl = arg
        elif opt in ("-p", "--path"):
            filePath = arg

    path = Path(filePath)

    loader = CSVLoader() if path.suffix == '.csv' else JSONLoader()
    loader.formPath(filePath)
    records = loader.load()

    uploadToStrapi(strapiUrl, path.stem, records)


if __name__ == "__main__":
    main()
